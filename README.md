## Prerequisites:
- terraform - [install](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- openstack-cli - [install](https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html)
- Form to access group project - https://projects.cloud.muni.cz/ (at least 1 Floating IP or more if you want use LoadBalancers, for master nodes we recommend to ask for `hpc.8core-16ram` )

## Create Infrastructure

Create openstack application credentials: 
 - On tab Identity > application credentials
 - \+ create application credential
 - Give a name > Create Applicaiton Credential
 - Download file! (The application credential secret will not be available after closing this page, so you must capture it now or download it. If you lose this secret, you must generate a new application credential)

```
git clone git@gitlab.ics.muni.cz:cloud/kubernetes/kubernetes-infra-example.git
cd kubernetes-infra-example-dev-ost
git submodule update --init
```
If required you may change the values as needed. (like infrastructure name, cluster size (master nodes, worker nodes groups), etc.) 
- `cd terraform/main.tf`
- Give a path to your public ssh key at: `ssh_public_key = "~/.ssh/id_rsa.pub"`

```
cd terraform/
source ~/path/to/credentials.sh
terraform init
terraform apply
```

## Create Kubernetes Cluster

#### Set parameters for the cluster

- In `../ansible/group_vars/` you can modify parameters for the cluster
- All possible group vars are in [Kubespray](https://github.com/kubernetes-sigs/kubespray) project.
- In `all/openstack.yml` modify the application credentials, which could be the same credentials as already created.

#### Install Kubernetes
Prerequisites: 
```
cd ../ansible/kubespray/
sudo pip install -r requirements.txt
```

Run ansible playbook for kubernetes:
```
cd ../ansible/kubespray/
ansible-playbook -i ../ansible_inventory --user=ubuntu --become --become-user=root cluster.yml
```

Run ansible additional playbook:
```
cd ../ansible/01-playbook/
ansible-playbook -i ../ansible_inventory --user=ubuntu --become --become-user=root play.yml
```

#### Access via config
- Kubeconfig is located in the artifacts directory, you can copy the config file to `~/.kube` directory for local access.

#### Access with SSH
- If you need to access the cluster
- Bastion ansible_hosts at `~/kubernetes-infra-example/ansible/ansible_inventory`
```
ssh -J ubuntu@<bastion_ip_address> ubuntu@<control-node_ip_address>
# For example:
# ssh -J ubuntu@195.113.167.169 ubuntu@10.10.10.26
sudo -i
kubectl get nodes
```