output "kubernetes_infra_bastion_external_ip" {
  value = module.kubernetes_infra.bastion_external_ip
}

output "kubernetes_infra_control_instance_ip" {
  value = module.kubernetes_infra.control_instance_ip
}

output "kubernetes_infra_worker_instance_ip" {
  value = module.kubernetes_infra.worker_instance_ip
}

output "kubernetes_infra_internal_vip" {
  value = module.kubernetes_infra.vip_ip
}

output "kubernetes_infra_floating_vip" {
  value = module.kubernetes_infra.vip_fip
}