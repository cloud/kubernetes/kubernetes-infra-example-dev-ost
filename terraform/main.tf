module "kubernetes_infra" {

  #  source = "./../../kubernetes-infra-dev-ost"
  source = "git::https://gitlab.ics.muni.cz/cloud/terraform/modules/kubernetes-infra-dev-ost.git?ref=1.1.1"

  # Example of variable override
  ssh_public_key = "~/.ssh/id_rsa.pub"

  infra_name = "infra-name"
  
  control_nodes_count       = 3
  control_nodes_volume_size = 30
  control_nodes_flavor      = "c3.8core-16ram" # brno hpc.8core-16ram

  additional_control_node_volume_size = 200

  bastion_flavor = "g2.tiny"
 
  worker_nodes = [
    {
      name        = "workers"
      flavor      = "g2.medium"
      volume_size = 30
      count       = 2
    }
  ]
  # Ostrava 
  public_external_network = "external-ipv4-general-public"

  # If you are using this modul for install kubespray
  kube_vip             = "10.0.0.5"
  kube_fip             = true
  kube_fip_create_port = true
}