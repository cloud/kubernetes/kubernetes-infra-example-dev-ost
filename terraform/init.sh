#!/bin/bash
PROJECT_NAME=$(openstack application credential show ${OS_APPLICATION_CREDENTIAL_ID} -f value -c name)
INFRA_NAME=$(grep -oP 'infra_name\s*=\s*"\K[^"]+' main.tf | awk '{print $1}')
# Check if the container already exists
if openstack container show "$PROJECT_NAME-$INFRA_NAME-tf-backend" >/dev/null 2>&1; then
    echo "Container already exists, skipping creation."
else
    # Container doesn't exist, create it
    openstack container create "$PROJECT_NAME-$INFRA_NAME-tf-backend"
    echo "Container created."
fi
# Check if EC2 credentials already exist
existing_credentials=$(openstack ec2 credentials list -f value | grep -c '^')
if [ "$existing_credentials" -gt 0 ]; then
    access=$(openstack ec2 credentials list -f value | head -n 1 |  awk '{print $1}')
    secret=$(openstack ec2 credentials list -f value | head -n 1 |  awk '{print $2}')
    echo "EC2 credentials already exist, skipping creation."
else
    # Create new EC2 credentials
    CREDENTIALS=$(openstack ec2 credentials create -f shell)
    access=$(echo "$CREDENTIALS" | grep -o 'access="[^"]*"' | cut -d'"' -f2)
    secret=$(echo "$CREDENTIALS" | grep -o 'secret="[^"]*"' | cut -d'"' -f2)
    echo "EC2 credentials created."
fi
if [ -f ".tf-s3-creds" ]; then
    echo "Credential file already exists, skipping creation."
else
    cat > .tf-s3-creds << EOL
    [default]
    aws_access_key_id=${access}
    aws_secret_access_key=${secret}
EOL
    echo "Credential file created."
fi
cat > backend.tf << EOL
 terraform { 
  backend "s3" {
    endpoints =                 { s3 = "https://object-store.cloud.muni.cz/"}
    shared_credentials_files    = ["./.tf-s3-creds"]
    bucket                      = "$PROJECT_NAME-$INFRA_NAME-tf-backend"
    use_path_style              = true
    key                         = "terraform.tfstate"
    region                      = "brno1"
    skip_credentials_validation = true
    skip_region_validation      = true
    skip_requesting_account_id  = true
    skip_metadata_api_check     = true
    skip_s3_checksum            = true
  }
} 
EOL